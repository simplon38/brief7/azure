variable "grp_name" {
  default = "brief7-quentin-k8s"
}

variable "location" {
  default = "northeurope"
}

# kubernetes
variable "agent_count" {
  default = 3
}

variable "cluster_name" {
  default = "quentin-k8s"
}

variable "dns_prefix" {
  default = "quentin-dns"
}
