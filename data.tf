resource "azurerm_ssh_public_key" "ssh_key" {
  name                = "ssh_key_admin"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  public_key          = file("~/.ssh/id_rsa.pub")
}

resource "random_string" "password" {
  length = 16
  special = true
  override_special = "/@\" "
}
